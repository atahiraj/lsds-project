from functools import wraps
from threading import Timer

# Helper classes
class Atomic:
    def __init__(self, get_lock: callable):
        self.get_lock = get_lock

    def __call__(self, method: callable):
        @wraps(method)
        def wrapper(*args: list, **kwargs: dict):
            lock = self.get_lock(args, kwargs)
            lock.acquire()
            ret = method(*args, **kwargs)
            lock.release()
            return ret

        return wrapper


class Scheduler:
    def __init__(self, method: callable):
        self.method = method
        self.timer = None

    def start(self, t: float, *args: list, **kwargs: dict):
        self.timer = Timer(t, self.method, args, kwargs)
        self.timer.start()

    def stop(self):
        if self.timer is not None:
            self.timer.cancel()

    def reset(self, t: float, *args: list, **kwargs: dict):
        self.stop()
        self.start(t, *args, **kwargs)
