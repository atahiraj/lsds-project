import random
import time
from threading import Thread, Event, Lock

from openapi_server.models.base_model_ import Model  # noqa: E501
from openapi_server.models.accept_response import AcceptResponse  # noqa: E501
from openapi_server.models.action import Action  # noqa: E501
from openapi_server.models.append_request import AppendRequest  # noqa: E501
from openapi_server.models.append_response import AppendResponse  # noqa: E501
from openapi_server.models.command import Command  # noqa: E501
from openapi_server.models.decide_response import DecideResponse  # noqa: E501
from openapi_server.models.entry import Entry  # noqa: E501
from openapi_server.models.node import Node  # noqa: E501
from openapi_server.models.state import State  # noqa: E501
from openapi_server.models.vote_request import VoteRequest  # noqa: E501
from openapi_server.models.vote_response import VoteResponse  # noqa: E501
from .raft import Raft


class FlightComputerRaft(Raft):
    def __init__(self, id: int, state: dict):
        super().__init__(id)
        self.state = State.from_dict(state)
        self.current_stage_index = 0
        self.completed = True
        self.stage_handlers = [
            self._handle_stage_1,
            self._handle_stage_2,
            self._handle_stage_3,
            self._handle_stage_4,
            self._handle_stage_5,
            self._handle_stage_6,
            self._handle_stage_7,
            self._handle_stage_8,
            self._handle_stage_9,
        ]
        self.stage_handler = self.stage_handlers[self.current_stage_index]

    def acceptable_state(self, state: State):
        return AcceptResponse(accepted = True)

    def decide_on_state(self, state: State):
        # Short circuit
        command = Command(name="deliver_state", value=state)
        self.client_request(command)
        return DecideResponse(decided=True)

        # Go through peers first
        """
        decided = self.acceptable_all(state, "computer/acceptable_state", self.acceptable_state)
        if decided:
            command = Command(name="deliver_state", value=state)
            self.client_request(command)

        return DecideResponse(decided=decided)
        """

    def sample_next_action(self):
        return self.stage_handler()

    def acceptable_action(self, action: Action):
        our_action = self.sample_next_action().to_dict()
        their_action = action.to_dict()
        accepted = True
        for k in our_action.keys():
            if our_action[k] != their_action[k]:
                accepted = False

        response = AcceptResponse(accepted=accepted)
        return response

    def decide_on_action(self, action: Action):
        decided = self.acceptable_all(action, "computer/acceptable_action", self.acceptable_action)
        if decided:
            command = Command(name="deliver_action", value=action)
            self.client_request(command)

        return DecideResponse(decided=decided)

    def acceptable_all(self, query: Model, endpoint: str, predicate: callable):
        counter = [0] * 2
        event = Event()
        lock = Lock()
        # Ask all peers
        for peer in self.peers.keys():
            Thread(
                target=self.request,
                args=[
                    "post",
                    int(peer),
                    endpoint,
                    query,
                    lambda response: self.acceptable_response(
                        response, counter, event, lock
                    ),
                    AcceptResponse
                ],
            ).start()
        # Ask self
        Thread(
            target=lambda: self.acceptable_response(
                predicate(query),
                counter,
                event,
                lock
            )
        ).start()

        event.wait()
        return counter[0] > counter[1]

    def acceptable_response(
        self, response: dict, counter: list, event: Event, lock: Lock
    ):
        lock.acquire()
        if response.accepted:
            counter[0] += 1
        else:
            counter[1] += 1
        criterion = int((self.nb_peers + 1) / 2)
        if counter[0] > criterion or counter[1] > criterion:
            event.set()
        lock.release()

    def apply_command(self, command: dict):
        if command.name == "deliver_state":
            self.state = command.value
        elif command.name == "deliver_action":
            action = command.value
            if hasattr(action, "next_stage") and action.next_stage:
                self.current_stage_index += 1
                self.stage_handler = self.stage_handlers[
                    self.current_stage_index
                ]

    # Handlers
    def _handle_stage_1(self):
        action = Action(
            pitch=90,
            throttle=1.0,
            heading=90,
            stage=False,
            next_state=False,
        )
        if self.state.altitude >= 1000:
            action.pitch = 80
            action.next_stage = True

        return action

    def _handle_stage_2(self):
        action = Action(
            pitch=80,
            throttle=1.0,
            heading=90,
            stage=False,
            next_state=False,
        )
        # Eject SRB's before the gravity turn
        if self.state.fuel_srb <= 1250:
            action.stage = True
            action.next_stage = True

        return action

    def _handle_stage_3(self):
        action = Action(
            pitch=80,
            throttle=1.0,
            heading=90,
            stage=False,
            next_state=False,
        )
        # Eject 2nd SRB + Initial Gravity turn
        if self.state.fuel_srb <= 10:
            action.stage = True
            action.pitch = 60.0
            action.next_stage = True

        return action

    def _handle_stage_4(self):
        action = Action(
            pitch=80,
            throttle=1.0,
            heading=90,
            stage=False,
            next_state=False,
        )
        # Turn
        if self.state.altitude >= 25000:
            action.pitch = 0
            action.throttle = 0.75
            action.next_stage = True

        return action

    def _handle_stage_5(self):
        action = Action(
            pitch=0,
            throttle=0.75,
            heading=90,
            stage=False,
            next_state=False,
        )
        # Cut throttle when apoapsis is 100km
        if self.state.apoapsis >= 100000:
            action.throttle = 0.0
            action.next_stage = True

        return action

    def _handle_stage_6(self):
        action = Action(
            pitch=0,
            throttle=0.0,
            heading=90,
            stage=False,
            next_state=False,
        )
        # Drop stage
        if self.state.altitude >= 80000:
            action.stage = True
            action.next_stage = True

        return action

    def _handle_stage_7(self):
        action = Action(
            pitch=0,
            throttle=0.0,
            heading=90,
            stage=False,
            next_state=False,
        )
        # Poor man's circularisation
        if self.state.altitude >= 100000:
            action.throttle = 1.0
            action.next_stage = True

        return action

    def _handle_stage_8(self):
        action = Action(
            pitch=0,
            throttle=1.0,
            heading=90,
            stage=False,
            next_state=False,
        )
        if self.state.periapsis >= 90000:
            action.throttle = 0.0
            action.next_stage = True

        return action

    def _handle_stage_9(self):
        self.completed = True


class FullThrottleFlightComputerRaft(FlightComputerRaft):
    def __init__(self, id: int, state: dict):
        super().__init__(id, state)

    def sample_next_action(self):
        action = super(
            FullThrottleFlightComputerRaft, self
        ).sample_next_action()
        action.throttle = 1.0

        return action


class RandomThrottleFlightComputerRaft(FlightComputerRaft):
    def __init__(self, id: int, state: dict):
        super().__init__(id, state)

    def sample_next_action(self):
        action = super(
            RandomThrottleFlightComputerRaft, self
        ).sample_next_action()
        action.throttle = random.uniform(0, 1)

        return action


class SlowFlightComputerRaft(FlightComputerRaft):
    def __init__(self, id: int, state: dict):
        super().__init__(id, state)

    def sample_next_action(self):
        action = super(SlowFlightComputerRaft, self).sample_next_action()
        time.sleep(random.uniform(0, 1) * 10)  # Seconds

        return action


class CrashingFlightComputerRaft(FlightComputerRaft):
    def __init__(self, id: int, state: dict):
        super().__init__(id, state)

    def sample_next_action(self):
        action = super(CrashingFlightComputerRaft, self).sample_next_action()
        # 1% probability of a crash
        if random.unifom(0, 1) <= 0.01:
            exit(1)

        return action


def new_flight_computer(id: int, state: dict, faulty: bool):
    if not faulty:
        return FlightComputerRaft(id, state)
    else:
        computer_types = [
            FullThrottleFlightComputerRaft,
            RandomThrottleFlightComputerRaft,
            SlowFlightComputerRaft,
            CrashingFlightComputerRaft,
        ]

        return computer_types[random.randint(0, 3)](id, state)
