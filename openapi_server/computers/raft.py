import logging
import requests_unixsocket
import random
from threading import Thread, Lock, Event

from openapi_server.models.base_model_ import Model  # noqa: E501
from openapi_server.models.accept_response import AcceptResponse  # noqa: E501
from openapi_server.models.action import Action  # noqa: E501
from openapi_server.models.append_request import AppendRequest  # noqa: E501
from openapi_server.models.append_response import AppendResponse  # noqa: E501
from openapi_server.models.command import Command  # noqa: E501
from openapi_server.models.decide_response import DecideResponse  # noqa: E501
from openapi_server.models.entry import Entry  # noqa: E501
from openapi_server.models.node import Node  # noqa: E501
from openapi_server.models.state import State  # noqa: E501
from openapi_server.models.vote_request import VoteRequest  # noqa: E501
from openapi_server.models.vote_response import VoteResponse  # noqa: E501

from .utils import Atomic, Scheduler

D_ELEC = 0.15
D_VOTE = 0.075


class Raft:
    FOLLOWER = 0
    CANDIDATE = 1
    LEADER = 2

    atomic = Atomic(lambda args, kwargs: args[0].lock)

    def __init__(self, id: int):
        self.id = id
        self.nb_peers = 0
        self.peers = {}
        self.crashed = {}
        self.election_timer = Scheduler(self.election_timeout)
        self.rpc_timers = {}
        self.events = {}
        self.lock = Lock()

        # Raft implementation
        # Every node starts as a follower
        self.raft_state = Raft.FOLLOWER
        # Set to 1 at first election when the application is fired up
        self.current_term = 0
        self.voted_for = None
        # Logs, starts at 0, not 1!!!
        self.log = []
        # Index of highest log entry known to be committed
        self.commit_index = -1
        # Last commit applied to state machine
        self.last_applied = -1
        # ID of the leader
        self.leader = None
        # Set of nodes that granted their votes
        self.votes = set()
        # Index of the next log entry to send to other nodes, starts at 0
        self.next_index = {}
        # Tndex of highest log entry known to be replicated on other nodes, starts at -1
        self.match_index = {}

    @atomic
    def add_peer(self, node: Node):
        peer = node.id
        self.peers[peer] = node.unix_socket
        self.crashed[peer] = False
        self.next_index[peer] = 0
        self.match_index[peer] = -1
        self.rpc_timers[peer] = Scheduler(self.rpc_timeout)
        self.nb_peers += 1

    @atomic
    def get_leader(self):
        if self.leader == self.id:
            unix_socket = f"/tmp/lsds{self.id}.socket"
        else:
            unix_socket = self.peers[self.leader]
        response = Node(id=self.leader, unix_socket=unix_socket)
        return response

    def client_request(self, request: Command):
        if self.raft_state == Raft.LEADER:
            # Appending and propagating command
            self.lock.acquire()
            if self.nb_peers > 0:
                new_event = Event()
                self.events[len(self.log)] = new_event

            entry = Entry(term=self.current_term, command=request)
            self.log.append(entry)
            self.reset_rpc_timers(0)

            self.lock.release()

            # Waiting for response
            if self.nb_peers > 0:
                new_event.wait()

            self.reset_rpc_timers(0)

    @atomic
    def append_request(self, request: AppendRequest):
        if request.term > self.current_term:
            self.stepdown(request.term)

        if request.term < self.current_term:
            response = AppendResponse(
                peer=self.id,
                term=self.current_term,
                success=False,
                index=-1,
            )
            return response
        else:
            self.leader = request.peer
            self.raft_state = Raft.FOLLOWER
            self.election_timer.reset(random.uniform(1, 2) * D_ELEC)
            success = request.prev_index == -1 or (
                request.prev_index <= len(self.log) - 1
                and self.log_term(request.prev_index) == request.prev_term
            )
            if success:
                index = request.prev_index
                for entry in request.entries:
                    index += 1
                    if self.log_term(index) != entry.term:
                        self.log.append(entry)

                self.commit_index = min(request.leader_commit, index)
                while self.last_applied < self.commit_index:
                    self.last_applied += 1
                    # Apply to state machine
                    self.apply_command(self.log[self.last_applied].command)
            else:
                index = -1

        response = AppendResponse(
            peer=self.id, term=self.current_term, success=success, index=index
        )
        return response

    @atomic
    def append_response(self, response: AppendResponse):
        peer = response.peer
        if self.current_term < response.term:
            self.stepdown(response.term)
        elif (
            self.raft_state == Raft.LEADER
            and self.current_term == response.term
        ):
            if response.success:
                self.match_index[peer] = response.index
                self.next_index[peer] = response.index + 1

                # We might need to update commit_index
                indices = list(self.match_index.values()) + [len(self.log) - 1]
                indices = sorted(indices)
                n = indices[int((self.nb_peers + 1) / 2)]
                if (
                    self.raft_state == Raft.LEADER
                    and self.log_term(n) == self.current_term
                ):
                    self.commit_index = n
                    while self.last_applied < self.commit_index:
                        self.last_applied += 1
                        # Apply to state machine
                        self.apply_command(self.log[self.last_applied].command)

                        # Send answer to client
                        if self.last_applied in self.events:
                            self.events[self.last_applied].set()
                            del self.events[self.last_applied]
            else:
                self.next_index[peer] = max(0, self.next_index[peer] - 1)

    @atomic
    def vote_request(self, request: VoteRequest):
        if request.term > self.current_term:
            self.stepdown(request.term)

        if (
            request.term == self.current_term
            and self.voted_for in (request.peer, None)
            and (
                request.last_log_term > self.log_term(len(self.log) - 1)
                or (
                    request.last_log_term == self.log_term(len(self.log) - 1)
                    and request.last_log_index >= len(self.log) - 1
                )
            )
        ):

            self.voted_for = request.peer
            vote_granted = True
            self.election_timer.reset(random.uniform(1, 2) * D_ELEC)
        else:
            vote_granted = False
        response = VoteResponse(
            peer=self.id,
            term=request.term,
            vote_granted=vote_granted,
        )
        return response

    @atomic
    def vote_response(self, response: VoteResponse):
        if response.term > self.current_term:
            self.stepdown(response.term)

        if (
            response.term == self.current_term
            and self.raft_state == Raft.CANDIDATE
        ):
            if response.vote_granted:
                self.votes.add(response.peer)

            if len(self.votes) > (self.nb_peers + 1) / 2:
                logging.info(f"Node {self.id}: is the leader")
                self.raft_state = Raft.LEADER
                self.leader = self.id
                self.reset_rpc_timers(0)

    @atomic
    def election_timeout(self):
        self.election_timer.reset(random.uniform(1, 2) * D_ELEC)
        if self.raft_state in (Raft.FOLLOWER, Raft.CANDIDATE):
            self.current_term += 1
            self.raft_state = Raft.CANDIDATE
            self.voted_for = self.id
            self.votes = {self.id}
            # For lone nodes
            if self.nb_peers == 0:
                logging.info(f"Node {self.id}: is the leader")
                self.raft_state = Raft.LEADER
                self.leader = self.id
            self.reset_rpc_timers(0)

    def rpc_timeout(self, peer: int):
        if self.raft_state == Raft.CANDIDATE:
            self.rpc_timers[peer].reset(D_VOTE, peer)
            request = VoteRequest(
                peer=self.id,
                term=self.current_term,
                last_log_term=self.log_term(len(self.log) - 1),
                last_log_index=len(self.log) - 1,
            )
            Thread(
                target=self.request,
                args=[
                    "post",
                    peer,
                    "raft/vote_request",
                    request,
                    self.vote_response,
                    VoteResponse,
                ],
            ).start()
        if self.raft_state == Raft.LEADER:
            self.rpc_timers[peer].reset(D_ELEC / 2, peer)
            prev_index = self.next_index[peer] - 1
            request = AppendRequest(
                peer=self.id,
                term=self.current_term,
                prev_index=prev_index,
                prev_term=self.log_term(prev_index),
                leader_commit=self.commit_index,
                entries=self.log[self.next_index[peer] :],
            )
            Thread(
                target=self.request,
                args=[
                    "post",
                    peer,
                    "raft/append_request",
                    request,
                    self.append_response,
                    AppendResponse,
                ],
            ).start()

    def apply_command(self, command: dict):
        pass

    # Helpers
    def reset_rpc_timers(self, t: float):
        for peer in self.peers.keys():
            self.rpc_timers[peer].reset(t, int(peer))

    def stepdown(self, term):
        self.current_term = term
        self.raft_state = Raft.FOLLOWER
        self.voted_for = None
        self.election_timer.reset(random.uniform(1, 2) * D_ELEC)

    def log_term(self, index):
        if index < 0 or index > len(self.log) - 1:
            return 0
        else:
            return self.log[index].term

    def request(
        self,
        method: str,
        peer: int,
        endpoint: str,
        data: Model,
        callback: callable,
        response_model: type,
    ):
        unix_socket = self.peers[peer].replace("/", "%2F")
        with requests_unixsocket.Session() as session:
            try:
                response = getattr(session, method)(
                    f"http+unix://{unix_socket}/{endpoint}",
                    json=data.to_dict() if data else None,
                )
                response = response_model.from_dict(response.json())
                if self.crashed[peer]:
                    self.lock.acquire()
                    self.crashed[peer] = False
                    self.nb_peers += 1
                    self.lock.release()

            except:  # Any networking error
                if not self.crashed[peer]:
                    logging.warn(f"Node {self.id}: Node {peer} crashed")
                    self.lock.acquire()
                    self.crashed[peer] = True
                    self.nb_peers -= 1
                    self.lock.release()
                    return
        callback(response)
