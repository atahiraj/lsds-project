# coding: utf-8

from __future__ import absolute_import
import unittest

from flask import json
from six import BytesIO

from openapi_server.models.accept_response import AcceptResponse  # noqa: E501
from openapi_server.models.action import Action  # noqa: E501
from openapi_server.models.append_request import AppendRequest  # noqa: E501
from openapi_server.models.append_response import AppendResponse  # noqa: E501
from openapi_server.models.command import Command  # noqa: E501
from openapi_server.models.decide_response import DecideResponse  # noqa: E501
from openapi_server.models.node import Node  # noqa: E501
from openapi_server.models.state import State  # noqa: E501
from openapi_server.models.vote_request import VoteRequest  # noqa: E501
from openapi_server.models.vote_response import VoteResponse  # noqa: E501
from openapi_server.test import BaseTestCase


class TestDefaultController(BaseTestCase):
    """DefaultController integration test stubs"""

    def test_acceptable_action(self):
        """Test case for acceptable_action

        Asks the node whether an action is acceptable
        """
        action = {
            "next_state": false,
            "throttle": 1.0,
            "stage": false,
            "heading": 90,
            "next_stage": false,
            "pitch": 90,
        }
        headers = {
            "Accept": "application/json",
            "Content-Type": "application/json",
        }
        response = self.client.open(
            "/computer/acceptable_action",
            method="POST",
            headers=headers,
            data=json.dumps(action),
            content_type="application/json",
        )
        self.assert200(
            response, "Response body is : " + response.data.decode("utf-8")
        )

    def test_acceptable_state(self):
        """Test case for acceptable_state

        Asks the node whether a state is acceptable
        """
        state = {
            "altitude": 86.89173985889647,
            "throttle": 0.0,
            "fuel_srb": 3169.844482421875,
            "fuel_s1": 2880.0,
            "periapsis": -598434.621188277,
            "fuel_s2": 720.0,
            "apoapsis": 87.46567809942644,
        }
        headers = {
            "Accept": "application/json",
            "Content-Type": "application/json",
        }
        response = self.client.open(
            "/computer/acceptable_state",
            method="POST",
            headers=headers,
            data=json.dumps(state),
            content_type="application/json",
        )
        self.assert200(
            response, "Response body is : " + response.data.decode("utf-8")
        )

    def test_add_peer(self):
        """Test case for add_peer

        Add a peer to this node
        """
        node = {"unix_socket": "/tmp/lsds.socket", "id": 2}
        headers = {
            "Content-Type": "application/json",
        }
        response = self.client.open(
            "/raft/peer",
            method="POST",
            headers=headers,
            data=json.dumps(node),
            content_type="application/json",
        )
        self.assert200(
            response, "Response body is : " + response.data.decode("utf-8")
        )

    def test_append_request(self):
        """Test case for append_request

        Send an append entries request to the node
        """
        append_request = {
            "prev_term": 3,
            "entries": [
                {
                    "term": 3,
                    "command": {
                        "name": "deliver_state",
                        "value": {
                            "altitude": 86.89173985889647,
                            "apoapsis": 87.46567809942644,
                            "periapsis": -598434.621188277,
                            "throttle": 0.0,
                            "fuel_srb": 3169.844482421875,
                            "fuel_s1": 2880.0,
                            "fuel_s2": 720.0,
                        },
                    },
                },
                {
                    "term": 3,
                    "command": {
                        "name": "deliver_state",
                        "value": {
                            "altitude": 86.89173985889647,
                            "apoapsis": 87.46567809942644,
                            "periapsis": -598434.621188277,
                            "throttle": 0.0,
                            "fuel_srb": 3169.844482421875,
                            "fuel_s1": 2880.0,
                            "fuel_s2": 720.0,
                        },
                    },
                },
            ],
            "leader_commit": 3,
            "peer": 3,
            "term": 3,
            "prev_index": 3,
        }
        headers = {
            "Accept": "application/json",
            "Content-Type": "application/json",
        }
        response = self.client.open(
            "/raft/append_request",
            method="POST",
            headers=headers,
            data=json.dumps(append_request),
            content_type="application/json",
        )
        self.assert200(
            response, "Response body is : " + response.data.decode("utf-8")
        )

    def test_client_request(self):
        """Test case for client_request

        Send a client request to the node
        """
        command = {
            "name": "deliver_state",
            "value": {
                "altitude": 86.89173985889647,
                "apoapsis": 87.46567809942644,
                "periapsis": -598434.621188277,
                "throttle": 0.0,
                "fuel_srb": 3169.844482421875,
                "fuel_s1": 2880.0,
                "fuel_s2": 720.0,
            },
        }
        headers = {
            "Content-Type": "application/json",
        }
        response = self.client.open(
            "/raft/client_request",
            method="POST",
            headers=headers,
            data=json.dumps(command),
            content_type="application/json",
        )
        self.assert200(
            response, "Response body is : " + response.data.decode("utf-8")
        )

    def test_decide_on_action(self):
        """Test case for decide_on_action

        Tell the node to decide on an action
        """
        action = {
            "next_state": false,
            "throttle": 1.0,
            "stage": false,
            "heading": 90,
            "next_stage": false,
            "pitch": 90,
        }
        headers = {
            "Accept": "application/json",
            "Content-Type": "application/json",
        }
        response = self.client.open(
            "/computer/decide_on_action",
            method="POST",
            headers=headers,
            data=json.dumps(action),
            content_type="application/json",
        )
        self.assert200(
            response, "Response body is : " + response.data.decode("utf-8")
        )

    def test_decide_on_state(self):
        """Test case for decide_on_state

        Tell the node to decide on a state
        """
        state = {
            "altitude": 86.89173985889647,
            "throttle": 0.0,
            "fuel_srb": 3169.844482421875,
            "fuel_s1": 2880.0,
            "periapsis": -598434.621188277,
            "fuel_s2": 720.0,
            "apoapsis": 87.46567809942644,
        }
        headers = {
            "Accept": "application/json",
            "Content-Type": "application/json",
        }
        response = self.client.open(
            "/computer/decide_on_state",
            method="POST",
            headers=headers,
            data=json.dumps(state),
            content_type="application/json",
        )
        self.assert200(
            response, "Response body is : " + response.data.decode("utf-8")
        )

    def test_get_leader(self):
        """Test case for get_leader

        Get the leader of the cluster
        """
        headers = {
            "Accept": "application/json",
        }
        response = self.client.open(
            "/raft/leader", method="GET", headers=headers
        )
        self.assert200(
            response, "Response body is : " + response.data.decode("utf-8")
        )

    def test_sample_next_action(self):
        """Test case for sample_next_action

        Get this node's next action
        """
        headers = {
            "Accept": "application/json",
        }
        response = self.client.open(
            "/computer/sample_next_action", method="GET", headers=headers
        )
        self.assert200(
            response, "Response body is : " + response.data.decode("utf-8")
        )

    def test_vote_request(self):
        """Test case for vote_request

        Send a vote request to the node
        """
        vote_request = {
            "last_log_term": 2,
            "peer": 3,
            "term": 1,
            "last_log_index": 2,
        }
        headers = {
            "Accept": "application/json",
            "Content-Type": "application/json",
        }
        response = self.client.open(
            "/raft/vote_request",
            method="POST",
            headers=headers,
            data=json.dumps(vote_request),
            content_type="application/json",
        )
        self.assert200(
            response, "Response body is : " + response.data.decode("utf-8")
        )


if __name__ == "__main__":
    unittest.main()
