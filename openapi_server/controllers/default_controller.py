import connexion
import six
from flask import current_app

from openapi_server.models.accept_response import AcceptResponse  # noqa: E501
from openapi_server.models.action import Action  # noqa: E501
from openapi_server.models.append_request import AppendRequest  # noqa: E501
from openapi_server.models.append_response import AppendResponse  # noqa: E501
from openapi_server.models.command import Command  # noqa: E501
from openapi_server.models.decide_response import DecideResponse  # noqa: E501
from openapi_server.models.node import Node  # noqa: E501
from openapi_server.models.state import State  # noqa: E501
from openapi_server.models.state_or_action import StateOrAction  # noqa: E501
from openapi_server.models.vote_request import VoteRequest  # noqa: E501
from openapi_server.models.vote_response import VoteResponse  # noqa: E501
from openapi_server import util


def acceptable_action(body):  # noqa: E501
    """Asks the node whether an action is acceptable

     # noqa: E501

    :param action: The action to decide on
    :type action: dict | bytes

    :rtype: AcceptResponse
    """
    if connexion.request.is_json:
        action = Action.from_dict(connexion.request.get_json())  # noqa: E501
    with current_app.app_context():
        flight_computer = current_app.flight_computer
    return flight_computer.acceptable_action(action)


def acceptable_state(body):  # noqa: E501
    """Asks the node whether a state is acceptable

     # noqa: E501

    :param state: The state to decide on
    :type state: dict | bytes

    :rtype: AcceptResponse
    """
    if connexion.request.is_json:
        state = State.from_dict(connexion.request.get_json())  # noqa: E501
    with current_app.app_context():
        flight_computer = current_app.flight_computer
    return flight_computer.acceptable_state(state)


def add_peer(body):  # noqa: E501
    """Add a peer to this node

     # noqa: E501

    :param node: Peer to add
    :type node: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        node = Node.from_dict(connexion.request.get_json())  # noqa: E501
    with current_app.app_context():
        flight_computer = current_app.flight_computer
    return flight_computer.add_peer(node)


def append_request(body):  # noqa: E501
    """Send an append entries request to the node

     # noqa: E501

    :param append_request: Append request
    :type append_request: dict | bytes

    :rtype: AppendResponse
    """
    if connexion.request.is_json:
        append_request = AppendRequest.from_dict(
            connexion.request.get_json()
        )  # noqa: E501

    for i in range(len(append_request.entries)):
        command = append_request.entries[i].command
        command.value = sanitize_state_or_action(command.value)

    with current_app.app_context():
        flight_computer = current_app.flight_computer

    return flight_computer.append_request(append_request)


def client_request(body):  # noqa: E501
    """Send a client request to the node

     # noqa: E501

    :param command: The command query
    :type command: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        command = Command.from_dict(connexion.request.get_json())  # noqa: E501
    command.value = sanitize_state_or_action(command.value)
    with current_app.app_context():
        flight_computer = current_app.flight_computer
    return flight_computer.client_request(command)


def decide_on_action(body):  # noqa: E501
    """Tell the node to decide on an action

     # noqa: E501

    :param action: The action to decide on
    :type action: dict | bytes

    :rtype: DecideResponse
    """
    if connexion.request.is_json:
        action = Action.from_dict(connexion.request.get_json())  # noqa: E501
    with current_app.app_context():
        flight_computer = current_app.flight_computer
    return flight_computer.decide_on_action(action)


def decide_on_state(body):  # noqa: E501
    """Tell the node to decide on a state

     # noqa: E501

    :param state: The state to decide on
    :type state: dict | bytes

    :rtype: DecideResponse
    """
    if connexion.request.is_json:
        state = State.from_dict(connexion.request.get_json())  # noqa: E501
    with current_app.app_context():
        flight_computer = current_app.flight_computer
    return flight_computer.decide_on_state(state)


def get_leader():  # noqa: E501
    """Get the leader of the cluster

     # noqa: E501


    :rtype: Node
    """
    with current_app.app_context():
        flight_computer = current_app.flight_computer
    return flight_computer.get_leader()


def sample_next_action():  # noqa: E501
    """Get this node&#39;s next action

     # noqa: E501


    :rtype: Action
    """
    with current_app.app_context():
        flight_computer = current_app.flight_computer
    return flight_computer.sample_next_action()


def vote_request(body):  # noqa: E501
    """Send a vote request to the node

     # noqa: E501

    :param vote_request: Vote request
    :type vote_request: dict | bytes

    :rtype: VoteResponse
    """
    if connexion.request.is_json:
        vote_request = VoteRequest.from_dict(
            connexion.request.get_json()
        )  # noqa: E501
    with current_app.app_context():
        flight_computer = current_app.flight_computer
    return flight_computer.vote_request(vote_request)

#Helper function
def sanitize_state_or_action(state_or_action: StateOrAction):
    if state_or_action.pitch is None:
        model = State
    else:
        model = Action
    return model.from_dict(state_or_action.to_dict())
