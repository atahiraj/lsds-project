#!/usr/bin/env python3
from openapi_server import run_app

if __name__ == "__main__":
    run_app(0, "/tmp/lsds.socket", None, False)
