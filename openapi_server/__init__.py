import bjoern
import connexion
import logging
import random
from flask import current_app

from openapi_server import encoder
from openapi_server.computers import new_flight_computer

def run_app(id: int, unix_socket: str, state: dict, faulty: bool):
    app = connexion.App(__name__, specification_dir="./openapi/")
    app.app.json_encoder = encoder.JSONEncoder
    app.add_api(
        "openapi.yaml", arguments={"title": "Raft node"}, pythonic_params=True
    )
    flight_computer = new_flight_computer(id, state, faulty)
    with app.app.app_context():
        current_app.flight_computer = flight_computer
    flight_computer.election_timer.reset(random.uniform(0.3, 1))
    logging.info(f"Node {id}: serving on http://unix:{unix_socket}")
    bjoern.run(app, f"unix:{unix_socket}")
