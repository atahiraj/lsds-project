# coding: utf-8

from __future__ import absolute_import
from datetime import date, datetime  # noqa: F401

from typing import List, Dict  # noqa: F401

from openapi_server.models.base_model_ import Model
from openapi_server import util


class VoteRequest(Model):
    """NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).

    Do not edit the class manually.
    """

    def __init__(
        self, peer=None, term=None, last_log_term=None, last_log_index=None
    ):  # noqa: E501
        """VoteRequest - a model defined in OpenAPI

        :param peer: The peer of this VoteRequest.  # noqa: E501
        :type peer: int
        :param term: The term of this VoteRequest.  # noqa: E501
        :type term: int
        :param last_log_term: The last_log_term of this VoteRequest.  # noqa: E501
        :type last_log_term: int
        :param last_log_index: The last_log_index of this VoteRequest.  # noqa: E501
        :type last_log_index: int
        """
        self.openapi_types = {
            "peer": int,
            "term": int,
            "last_log_term": int,
            "last_log_index": int,
        }

        self.attribute_map = {
            "peer": "peer",
            "term": "term",
            "last_log_term": "last_log_term",
            "last_log_index": "last_log_index",
        }

        self._peer = peer
        self._term = term
        self._last_log_term = last_log_term
        self._last_log_index = last_log_index

    @classmethod
    def from_dict(cls, dikt) -> "VoteRequest":
        """Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: The VoteRequest of this VoteRequest.  # noqa: E501
        :rtype: VoteRequest
        """
        return util.deserialize_model(dikt, cls)

    @property
    def peer(self):
        """Gets the peer of this VoteRequest.

        ID of the requester  # noqa: E501

        :return: The peer of this VoteRequest.
        :rtype: int
        """
        return self._peer

    @peer.setter
    def peer(self, peer):
        """Sets the peer of this VoteRequest.

        ID of the requester  # noqa: E501

        :param peer: The peer of this VoteRequest.
        :type peer: int
        """
        if peer is None:
            raise ValueError(
                "Invalid value for `peer`, must not be `None`"
            )  # noqa: E501

        self._peer = peer

    @property
    def term(self):
        """Gets the term of this VoteRequest.

        Current term of the requester  # noqa: E501

        :return: The term of this VoteRequest.
        :rtype: int
        """
        return self._term

    @term.setter
    def term(self, term):
        """Sets the term of this VoteRequest.

        Current term of the requester  # noqa: E501

        :param term: The term of this VoteRequest.
        :type term: int
        """
        if term is None:
            raise ValueError(
                "Invalid value for `term`, must not be `None`"
            )  # noqa: E501

        self._term = term

    @property
    def last_log_term(self):
        """Gets the last_log_term of this VoteRequest.

        Last log term of the requester  # noqa: E501

        :return: The last_log_term of this VoteRequest.
        :rtype: int
        """
        return self._last_log_term

    @last_log_term.setter
    def last_log_term(self, last_log_term):
        """Sets the last_log_term of this VoteRequest.

        Last log term of the requester  # noqa: E501

        :param last_log_term: The last_log_term of this VoteRequest.
        :type last_log_term: int
        """
        if last_log_term is None:
            raise ValueError(
                "Invalid value for `last_log_term`, must not be `None`"
            )  # noqa: E501

        self._last_log_term = last_log_term

    @property
    def last_log_index(self):
        """Gets the last_log_index of this VoteRequest.

        Last log index of the requester  # noqa: E501

        :return: The last_log_index of this VoteRequest.
        :rtype: int
        """
        return self._last_log_index

    @last_log_index.setter
    def last_log_index(self, last_log_index):
        """Sets the last_log_index of this VoteRequest.

        Last log index of the requester  # noqa: E501

        :param last_log_index: The last_log_index of this VoteRequest.
        :type last_log_index: int
        """
        if last_log_index is None:
            raise ValueError(
                "Invalid value for `last_log_index`, must not be `None`"
            )  # noqa: E501

        self._last_log_index = last_log_index
