# coding: utf-8

from __future__ import absolute_import
from datetime import date, datetime  # noqa: F401

from typing import List, Dict  # noqa: F401

from openapi_server.models.base_model_ import Model
from openapi_server import util


class AcceptResponse(Model):
    """NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).

    Do not edit the class manually.
    """

    def __init__(self, accepted=None):  # noqa: E501
        """AcceptResponse - a model defined in OpenAPI

        :param accepted: The accepted of this AcceptResponse.  # noqa: E501
        :type accepted: bool
        """
        self.openapi_types = {"accepted": bool}

        self.attribute_map = {"accepted": "accepted"}

        self._accepted = accepted

    @classmethod
    def from_dict(cls, dikt) -> "AcceptResponse":
        """Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: The AcceptResponse of this AcceptResponse.  # noqa: E501
        :rtype: AcceptResponse
        """
        return util.deserialize_model(dikt, cls)

    @property
    def accepted(self):
        """Gets the accepted of this AcceptResponse.


        :return: The accepted of this AcceptResponse.
        :rtype: bool
        """
        return self._accepted

    @accepted.setter
    def accepted(self, accepted):
        """Sets the accepted of this AcceptResponse.


        :param accepted: The accepted of this AcceptResponse.
        :type accepted: bool
        """
        if accepted is None:
            raise ValueError(
                "Invalid value for `accepted`, must not be `None`"
            )  # noqa: E501

        self._accepted = accepted
