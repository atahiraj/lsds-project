#!/bin/bash

sudo docker run --rm -v "${PWD}:/local" openapitools/openapi-generator-cli generate -i /local/openapi.yaml -g python-flask -o /local/.tmp
sudo chown --reference=gen.sh -R .tmp
find .tmp -type f -name "*.py" | xargs black -l 79
cp -r .tmp/openapi_server .
rm -rf .tmp
