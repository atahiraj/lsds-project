import argparse
import pickle
import logging
import math
import random
import time




from computers import FlightComputer

logging.basicConfig(level=logging.INFO)

actions = pickle.load(open("actions.pickle", "rb"))
states = pickle.load(open("states.pickle", "rb"))
timestep = 0

parser = argparse.ArgumentParser()
parser.add_argument("--correct-fraction", type=float, default=1.0, help="Fraction of correct flight computers (default 1.0).")
parser.add_argument("--flight-computers", type=int, default=3, help="Number of flight computers (default: 3).")
arguments, _ = parser.parse_known_args()

def readout_state():
    return states[timestep]

def execute_action(action):
    print(action)
    print(actions[timestep])
    for k in action.keys():
        assert(action[k] == actions[timestep][k])


def allocate_flight_computers(arguments):
    flight_computers = []
    n_fc = arguments.flight_computers
    n_correct_fc = math.ceil(arguments.correct_fraction * n_fc)
    n_incorrect_fc = n_fc - n_correct_fc
    state = readout_state()
    for _ in range(n_correct_fc):
        flight_computers.append(FlightComputer(state, False))
    for _ in range(n_incorrect_fc):
        flight_computers.append(FlightComputer(state, True))

    # Add the peers for the consensus protocol
    time.sleep(0.05 * n_fc)
    for fc in flight_computers:
        for peer in flight_computers:
            if fc != peer:
                fc.add_peer(peer)

    return flight_computers

# Connect with Kerbal Space Program
flight_computers = allocate_flight_computers(arguments)


time.sleep(1)

def select_leader():
    leader_index = random.randint(0, len(flight_computers))

    try:
        return flight_computers[leader_index].get_leader()
    except KeyboardInterrupt:
        exit(0)
    except:
        return select_leader()


def next_action(state):
    leader = select_leader()
    state_decided = leader.decide_on_state(state)
    if not state_decided:
        return None
    action = leader.sample_next_action()
    action_decided = leader.decide_on_action(action)
    if action_decided:
        return action

    return None

complete = False
try:
    while not complete:
        print(timestep)
        timestep += 1
        state = readout_state()
        leader = select_leader()
        state_decided = leader.decide_on_state(state)
        if not state_decided:
            continue
        action = leader.sample_next_action()
        if action is None:
            complete = True
            continue
        if leader.decide_on_action(action):
            execute_action(action)
        else:
            timestep -= 1
except KeyboardInterrupt:
    exit(0)
except Exception as e:
    print(e)

if complete:
    print("Success!")
else:
    print("Fail!")

"""
while True:
    pass
"""
