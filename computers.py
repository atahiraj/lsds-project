import os
import requests_unixsocket
from multiprocessing import Process

from openapi_server import run_app


class FlightComputer:
    ID = 0

    def __init__(self, state: dict, faulty: bool):
        self.id = FlightComputer.ID
        FlightComputer.ID += 1
        self.unix_socket = f"/tmp/lsds{self.id}.socket"
        self.nodes = dict()
        self.nodes[self.unix_socket] = self

        self.p = Process(
            target=run_app, args=[self.id, self.unix_socket, state, faulty]
        )
        self.p.daemon = True
        self.p.start()

    def add_peer(self, peer):
        self.nodes[peer.unix_socket] = peer
        data = {"id": peer.id, "unix_socket": peer.unix_socket}
        self.request("post", "raft/peer", data)

    def get_leader(self):
        response = self.request("get", "raft/leader", None)
        return self.nodes[response["unix_socket"]]

    def acceptable_state(self, state: dict):
        return True

    def decide_on_state(self, state: dict):
        response = self.request("post", "computer/decide_on_state", state)
        return response["decided"]

    def sample_next_action(self):
        response = self.request("get", "computer/sample_next_action", None)
        return response

    def acceptable_action(self, action: dict):
        response = self.request("post", "computer/acceptable_action", action)
        return response["accepted"]

    def decide_on_action(self, action: dict):
        response = self.request("post", "computer/decide_on_action", action)
        return response

    def request(self, method: str, endpoint: str, data: dict):
        unix_socket = self.unix_socket.replace("/", "%2F")
        with requests_unixsocket.Session() as session:
            response = getattr(session, method)(
                f"http+unix://{unix_socket}/{endpoint}", json=data
            )
        try:
            return response.json()
        except:
            return None
