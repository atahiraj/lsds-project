# LSDS Project

## TODO

- Integrate fusée truc là
- Add entrypoints to add nodes to cluster (isnt it an overkill ???)

## Setup

### Install docker
You need to have docker and docker-compose.
- https://docs.docker.com/get-docker/
- https://docs.docker.com/compose/install/

### Build the image
Build the image by running
```console
$ docker build -t lsds server/
```

## Usage
### Generating the docker-compose file
Run 
```console
$ ./docker-compose.sh nb_node
```
where *nb_node* is the number of nodes you want your cluster to have. 

### Run
Be sure to have the ports {8080,...,(8080 + *nb_node* - 1)} available. Run your cluster with
```console
$ docker-compose up
```

### Stop

Since CTRL + C stops the containers but does not remove them, it is best practice to remove the containers afterwards with
```console
$ docker-compose down -v
```

# Interact with the cluster

To interact with node *i*, go to localhost:$((8080 + i - 1))/ui to see examples of curl commands for each endpoint. E.g. for the second node, go to http://localhost:8081/ui.

To remove a node from the cluser, run
```console
$ docker kill container_name
```
where *container_name* is the name of the node's container. To see running containers, use
```console
$ docker ps
```
